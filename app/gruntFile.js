module.exports = function(grunt){

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            sample: {
                src: 'components/**/*.js'
            },
            options: {
                force: true
            }
        },
        htmlmin: {
            dist: {
                src: ['app/index.html', 'components/**/*.html'],
                dest: 'builds/development/html/index.html',
                options: {
                    removeComments: true,
                    collapseWhitespace: true
                }
            }
        },
        compress: {
            sample: {
                options: {
                    archive: 'losers.zip'
                },
                src: ['app/app.js', 'components/**/*.js']
            }

        },
        connect: {
            server: {
                options: {
                    hostname: '0.0.0.0',
                    port: 3000,
                    livereload: true,
                    base: './',
                    open: true
                }
            }
        },
        watch: {
            scripts: {
                files: ['app.js', 'scripts/**/*.js', 'styles/*.css', 'index.html', 'templates/**.html'],
                task: ['concat', 'jshint']
            },
            options: {
                spawn: false,
                livereload: true,
                interrupt: true
            }
        },
        concat : {
            dist : {
                src : ['app/app.js', 'components/**/*.js'],
                dest : 'builds/development/js/script_<%= pkg.version %>.js'
            },
            options: {
                stripBanners: true
            }
        },
        wiredep: {
            task: {
                src: 'index.html'
            }

        }
    }); //init config section

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-wiredep');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');

    grunt.registerTask('default', ['wiredep', 'jshint', 'concat', 'connect', 'watch']);
}; //wrapper function