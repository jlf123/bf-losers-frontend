angular.module('losers', [
    'ui.router',
    'ngAnimate',
    'ngResource',
    'ui.bootstrap',
    'matchMedia',
    'LocalStorageModule',
    'ngFileUpload',
    'ngLetterAvatar'
])
.constant('BASE_URI', 'http://localhost:5000')

.constant('_', window._)

.config(function($stateProvider, $urlRouterProvider, $httpProvider){
    
    $urlRouterProvider.otherwise('/login');

    $stateProvider
        .state('app', {
            templateUrl: 'templates/root.html'
        })
        .state('app.landing', {
            url: '/home',
            views: {
                'content@app' : {
                    url: '/landing',
                    templateUrl: 'templates/landing.html',
                    controller: 'landingCtrl'
                },
                'sidebar@app' : {
                    templateUrl: 'templates/sidebar.html',
                    controller: 'profileSidebarCtrl'
                }
            }
        })
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'loginCtrl'
        })
        .state('app.competitionview', {
            url: '/competition:comp',
            views: {
                'content@app' : {
                    templateUrl: 'templates/competitionview.html',
                    controller: 'CompetitionCtrl',
                    params: { competition: null }
                },
                'sidebar@app' : {
                    templateUrl: 'templates/sidebar.html',
                    controller: 'profileSidebarCtrl'
                }
            }

        })
    
    $httpProvider.interceptors.push('JWTInterceptor')
    
});