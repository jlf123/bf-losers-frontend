/**
 * Created by john.flournoy on 9/22/16.
 */
angular.module('losers')
    .controller('AddCompetitionCtrl', ['$scope', '$uibModalInstance', '$timeout', 'Users', 'Competitions', '_', 'Weighin',
        function($scope, $uibModalInstance, $timeout, Users, Competitions, _, Weighin){

            $scope.participants = []
            $scope.loading = false;
            $scope.errorMessage = null
            $scope.userList = null;
            $scope.competition = {
                end_date: Date.now()
            }

            $scope.close = function(){
                $uibModalInstance.dismiss('cancel')
            }

            $scope.StartCompetition = function(competition){
                $scope.loading = true;
                competition.participants = $scope.participants
                Competitions.save(competition, saveSuccess, saveFailure)
            }

            $scope.addUser = function(user, index){
                for(var i = 0; i < $scope.userList.length; i++){
                    if($scope.userList[i]._id === user._id){
                        $scope.userList.splice(i, 1);
                        break;
                    }
                }
                $scope.participants.push(user)
            }

            var saveSuccess = function(competition){
                $scope.loading = false;
                $uibModalInstance.close(true)
            }

            var saveFailure = function(error){
                $scope.errorMessage = "Please Add 2 or More Participants";
                $scope.loading = false;
            }

            var init = function(){
                Users.query({}, function(response){
                    $scope.userList = response
                })
            }

            init();

    }])

angular.module('losers').controller('joinCompetitionCtrl', ['$scope', '$uibModalInstance', 'Query',
    function($scope, $uibModalInstance, Query){

        $scope.sending = false;

        $scope.joinable = []

        $scope.close = function(){
            $uibModalInstance.dismiss()
        }

        $scope.getJoinableCompetitions = function(search){
            Query.query({
                type: search.searchby,
                search: search.query
            }, function(response){
                $scope.joinable = response;
                console.log(response)
            })

        }

}])

angular.module('losers')
    .controller('weighinCtrl', ['$scope', 'users', '$uibModalInstance', 'Weighin', 'competition', 'IndividualWeighin', '$rootScope',
        function($scope, users, $uibModalInstance, Weighin, competition, IndividualWeighin, $rootScope){

            $scope.users = users.participants ? users.participants : users;
            $scope.isOwner = users.isOwner;
            $scope.currentUser = users.user;
            $scope.selectedUser = null
            $scope.usersready = users.usersready ? users.usersready.individual_weighins : [] ;
            $scope.readonly = false;
            $scope.loading = false;
            $scope.competition = competition
            $scope.showDeleteAlert = false;
            var update = false;
            var weighinId = users.usersready ? users.usersready._id : null ;

            var removeUserById = function(id){
                for(var i = 0; i < $scope.users.length; i++){
                    if($scope.users[i]._id === id){
                        $scope.users.splice(i, 1);
                        $scope.selectedUser = !$scope.users.length ? null : $scope.users[0]
                        return;
                    }
                }
            }

            $scope.edit = function(){
                $scope.readonly = false;
                var found;
                if($scope.isOwner){
                    angular.forEach($scope.users, function(user){
                        found = false;
                        for(var i = 0; i < $scope.usersready.length; i++){
                            if(user._id === $scope.usersready[i]._id){
                                user.newWeight = $scope.usersready[i].newWeight
                                user.userweighinId = $scope.usersready[i].userweighinId
                                found = true;
                            }
                        }
                        if(!found){ user.newWeight = null }
                    })
                    $scope.selectedUser = $scope.users[0];
                    $scope.usersready = [];
                } else {
                    $scope.users = [];
                    angular.forEach($scope.usersready, function(entity, index){
                        if(entity._id === $scope.currentUser._id){
                            $scope.users[0] = $scope.currentUser;
                            $scope.users[0].userweighinId = $scope.usersready[index].userweighinId
                            $scope.usersready.splice(index, 1)
                        }
                    })
                    $scope.selectedUser = $scope.users[0];
                    console.log($scope.currentUser)
                }

            }

            $scope.$on('event:saveweight', function(event, user){
                $scope.add(user);
            })

            $scope.setUser = function(user){
                $scope.selectedUser = user;
            }

            $scope.add = function(user){
                $scope.usersready.push(user);
                removeUserById(user._id)
            }

            $scope.finish = function(){
                var requestbody = []
                angular.forEach($scope.usersready, function(user){
                    requestbody.push({
                        participant: user._id,
                        weigh_in_weight: user.newWeight === "TBD" ? 0 : user.newWeight,
                        userweighinId: user.userweighinId
                    })
                })
                $scope.loading = true;
                return update ? updateWeighinHandler(requestbody) : saveWeighinHandler(requestbody)
            }

            $scope.delete = function(){
                $scope.loading = true;
                Weighin.delete({
                    competitionId: $scope.competition._id,
                    weighinId: weighinId
                }, function(weighin){
                    $scope.loading = false;
                    $rootScope.$broadcast('event:weighinupdated', true, 'Weighin Deleted!')
                    $uibModalInstance.close(true)
                }, function(error){
                    $scope.loading = false;
                    $rootScope.$broadcast('event:weighinupdated', false, error)
                    $uibModalInstance.close(true)
                })
            }

            var saveWeighinHandler = function(requestbody){
                Weighin.save({
                    competitionId: $scope.competition._id
                }, {
                    user_weighins : requestbody
                }, function(competition){
                    $scope.loading = false;
                    $rootScope.$broadcast('event:weighinupdated', true, 'Weighin Saved!')
                    $uibModalInstance.close(competition)
                }, function(err){
                    $rootScope.$broadcast('event:weighinupdated', false, err)
                    $scope.loading = false;
                    $uibModalInstance.close(competition)
                })
            }

            var updateWeighinHandler = function(requestbody, userweighinId){
                angular.forEach(requestbody, function(info, index){

                    IndividualWeighin.update({
                        indvWeighinId: info.userweighinId
                    }, info, function(userWeighin){
                        if(index + 1 == requestbody.length){
                            $rootScope.$broadcast('event:weighinupdated', true, 'Weighin Updated!')
                            $uibModalInstance.close(true)
                        }
                    }, function(err){
                        $rootScope.$broadcast('event:weighinupdated', false, err)
                        $scope.loading = false;
                        $uibModalInstance.close(competition)
                    })

                })
            }

            $scope.close = function(){
                $scope.usersready = [];
                $scope.users = [];
                $uibModalInstance.close()
            }

            var init = function(){
                if($scope.usersready.length){
                    $scope.readonly = true;
                    update = true;
                    var tmp = $scope.usersready
                    $scope.usersready = []
                    angular.forEach(tmp, function(_weighin){
                        $scope.usersready.push({
                            _id: _weighin.participant._id,
                            userweighinId: _weighin._id,
                            firstName: _weighin.participant.firstName,
                            newWeight: _weighin.weigh_in_weight
                        })
                    })

                }
            }

            init()

        }])