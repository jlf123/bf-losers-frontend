/**
 * Created by john.flournoy on 9/11/16.
 */

angular.module('losers')
    .controller('landingCtrl', [ '$scope', '$timeout', '$uibModal', 'screenSize', 'Competitions', '$state', 'Request',
        function($scope, $timeout, $uibModal, screenSize, Competitions, $state, Request){
            $scope.register_alert_success = false;
            $scope.register_alert_failure = false;
            $scope.register_alert = '';
            $scope.competitions = null;
            $scope.requests = []

            $scope.screenType = screenSize.is('md, lg') ? 'Desktop' : 'Mobile';

            $scope.screenType = screenSize.on('md, lg', function(match){
                if(match){
                    $scope.screenType = 'Desktop';
                } else {
                    $scope.screenType = 'Mobile'
                }
            })


            $scope.$on('Profile_Saved', function(){
                $scope.register_alert = "Account Successfully Created";
                $scope.register_alert_success = true;
                $timeout(function(){
                    $scope.register_alert_success = false;
                }, 5000)
            })

            $scope.$on('Profile_Saved_Error', function(error){
                $scope.register_alert = error;
                $scope.register_alert_failure = true
                $timeout(function(){
                    $scope.register_alert_failure = false;
                }, 5000)
            })


            $scope.openStartModal = function(){
                var modalInstance = $uibModal.open({
                    animation: true,
                    size: 'lg',
                    templateUrl: 'startCompetitionModal.html',
                    controller: 'AddCompetitionCtrl'
                })
                modalInstance.result.then(function(saved){
                    if(saved){
                        init()
                        $scope.register_alert_success = true;
                        $scope.register_alert = "Competition Saved!"
                        $timeout(function(){
                            $scope.register_alert_success = $scope.register_alert = null;
                        },5000)
                    }
                })
            }

            $scope.$on('event:competitionsaved', function(event, success, message){
                if(success){
                    $scope.register_alert_success = true;
                    $scope.register_alert = message
                    init()
                    $timeout(function(){
                        $scope.register_alert_success = $scope.register_alert = null;
                    },5000)
                } else {
                    $scope.register_alert_success = false;
                    $scope.register_alert = message
                    $timeout(function(){
                        $scope.register_alert_success = $scope.register_alert = null;
                    },5000)
                }
            })

            $scope.openCompetitionView = function(competition){
                $state.go('app.competitionview', {
                    comp: JSON.stringify(competition)
                })
            }

            var init = function(){
                Competitions.query({}, function(response){
                    if(response.length){
                        $scope.competitions = response
                    }
                })
                Request.query({}, function(requests){
                    $scope.requests = requests
                })
            }

            init();

    }])