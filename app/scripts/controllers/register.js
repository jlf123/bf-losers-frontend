angular.module('losers')
    .controller('RegisterCtrl', [ '$scope', 'Profile', '$timeout', 'Upload', 'BASE_URI', function($scope, Profile, $timeout, Upload, BASE_URI){

        $scope.register_info = {};
        $scope.uploadInProgress = false;

        $scope.register = function(form_info){
            console.log(form_info)

            Profile.save({
                username: form_info.username,
                password: form_info.password,
                firstName : form_info.firstName,
                lastName : form_info.lastName,
                email : form_info.email,
                website : form_info.website,
                weight : form_info.weight,
                avatar: form_info.avatar
            }, registerSuccess, registerFailure);
        };

        $scope.onUploadSelect = function(image) {
            if(image === null){ return; }
            console.log(image)
            $scope.uploadInProgress = true;
            $scope.uploadProgress = 0;

            if (angular.isArray(image)) {
                image = image[0];
            }

            $scope.upload = Upload.upload({
                url: BASE_URI + '/losers/upload/image',
                method: 'POST',
                data: {
                    type: 'profile'
                },
                file: image
            }).success(function(data, status, headers, config) {
                $scope.uploadInProgress = false;
                $scope.register_info.avatar = data
            }).error(function(err) {
                $scope.uploadInProgress = false;
                console.log('Error uploading file: ' + err.message || err);
            });
        };

        var registerSuccess = function(){
            $scope.register_alert_success = true;
            $scope.register_alert = "Profile successfully saved";
            $scope.showRegisterForm = false;
            $timeout(function(){
                $scope.register_alert_success = false;
            }, 5000)

        }

        var registerFailure = function(response){
            $scope.register_alert_failure = true;
            $scope.register_alert = "Unable to save profile"
            $timeout(function(){
                $scope.register_alert_failure = false;
            }, 5000)
        }

        $scope.formValidate = function(register_info){
            return (register_info && register_info.username && register_info.password
            && register_info.verifyPassword && register_info.weight && register_info.firstName && register_info.lastName)
        }
    
}])
