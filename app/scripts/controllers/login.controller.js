angular.module('losers')
    .controller('loginCtrl', [ '$scope', 'Profile', '$rootScope', '$state', 'Login', '$timeout', 'ProfileData', 'Sessions', 'localStorageService',
        function($scope, Profile, $rootScope, $state, Login, $timeout, ProfileData, Sessions, localStorageService){
            $scope.showRegisterForm = false;
            $scope.loggedIn = false;
            $scope.register_alert_success = false;
            $scope.register_alert_failure = false;
            $scope.register_alert = null;
            $scope.login_error = null;

            $scope.openRegisterForm = function(){
                if(!$scope.showRegisterForm){
                    $scope.showRegisterForm = true;
                }

            };

            var loginSuccess = function(response){
                localStorageService.set('JWT-Key', response.token)
                ProfileData.getProfile()
                $state.go('app.landing');

            }

            var loginFailure = function(response){
                if(response.status === 401){
                    $scope.login_error = "Invalid Credentials"
                }
            }

            $scope.login = function(loginData){

                Sessions.save({
                    username: loginData.username,
                    password: loginData.password
                }, loginSuccess, loginFailure)


            };
            
            

        }]);
