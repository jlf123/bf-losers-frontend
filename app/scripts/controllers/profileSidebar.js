'use strict';

angular.module('losers')
    .controller('profileSidebarCtrl', [ '$scope', 'ProfileData', '$state', 'Logout', '$uibModal', 'screenSize', 'localStorageService', '$compile', '$rootScope',
        function($scope, ProfileData, $state, Logout, $uibModal, screenSize, localStorageService, $compile, $rootScope){

            $scope.screenType = screenSize.is('md, lg') ? 'Desktop' : 'Mobile';
            $scope.user = 'test';

            $scope.screenType = screenSize.on('md, lg', function(match){
                if(match){
                    $scope.screenType = 'Desktop';
                } else {
                    $scope.screenType = 'Mobile'
                }
            })


            $scope.logout = function(){
                ProfileData.clear();
                localStorageService.remove('JWT-Key')
                $state.go('login');
            }

            var init = function(){
                ProfileData.getProfile().then(function(profile){
                    $scope.user = profile;
                })
            }

            $scope.openStartModal = function(){
                var modalInstance = $uibModal.open({
                    animation: true,
                    size: 'lg',
                    templateUrl: 'startCompetitionModal.html',
                    controller: 'AddCompetitionCtrl'
                })
                modalInstance.result.then(function(saved){
                    if(saved){
                        $rootScope.$broadcast('event:competitionsaved', true, "Competition Saved!")
                    }
                })
            }

            $scope.openJoinModal = function(){
                $uibModal.open({
                    animation: true,
                    size: 'lg',
                    templateUrl: '../../templates/joinmodal.html',
                    controller: 'joinCompetitionCtrl'
                })

            }

            init()

}]);
