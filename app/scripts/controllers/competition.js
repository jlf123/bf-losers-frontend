angular.module('losers').controller('CompetitionCtrl', ['$scope', '$stateParams', '$uibModal', 'Competitions',
    'ProfileData', '$timeout',
        function($scope, $stateParams, $uibModal, Competitions, ProfileData, $timeout){

            $scope.competition = null;
            $scope.isOwner = false;
            $scope.competitionalert = null;
            $scope.requests = null;

            var checkOwner = function(){
                $scope.isOwner = $scope.competition.creator._id === $scope.user._id;
            }

            var init = function(){
                $scope.competition = JSON.parse($stateParams.comp);
                ProfileData.getProfile().then(function(profile){
                    $scope.user = profile;
                })
                Competitions.get({
                    competitionId: $scope.competition._id
                }, function(_competition){
                    $scope.competition.weigh_ins = _competition.weigh_ins
                    checkOwner()
                })
            }

            $scope.openWeighIn = function(participants, owner){
                var tmp = participants.slice();
                tmp.push(owner)
                var modalInstance = $uibModal.open({
                    animation: true,
                    size: 'md',
                    templateUrl: 'templates/weighin.html',
                    controller: 'weighinCtrl',
                    resolve: {
                        users: function(){
                            return tmp
                        },
                        competition: function(){
                            return $scope.competition
                        }
                    }
                })

                modalInstance.result.then(function(){
                    console.log('here')
                    init() //
                })
            }

            $scope.viewWeighIn = function(_weighin, participants, owner){
                var tmp = participants.slice()
                tmp.push(owner)
                var modalInstance = $uibModal.open({
                    animation: true,
                    size: 'md',
                    templateUrl: 'templates/weighin.html',
                    controller: 'weighinCtrl',
                    resolve: {
                        users: function(){
                            return {
                                usersready: _weighin,
                                participants: tmp,
                                isOwner: $scope.isOwner,
                                user: $scope.user
                            }
                        },
                        competition: function(){
                            return $scope.competition
                        }
                    }
                })
            }

            $scope.$on('event:weighinupdated', function(event, success, message){
                if(success){
                    $scope.competitionalert = {
                        error: false,
                        message: message
                    }
                    $timeout(function(){
                        $scope.competitionalert = null
                    }, 5000)
                }
                init();
            })

            init();

}])