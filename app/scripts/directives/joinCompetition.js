/**
 * Created by john.flournoy on 2/18/17.
 */
'use strict';

angular.module('losers').directive('joinCompetition',[ 'Request', '$rootScope', function(Request, $rootScope){
    return {
        restrict: 'E',
        scope: {
            joinable: '=',
            sending: '='
        },
        templateUrl: 'templates/joincompetition.html',
        link: function(scope, el, attr){
            scope.requestsent = false;
            scope.alert = null

            scope.sendRequest = function(){
                scope.sending = true;
                Request.save({
                    decider: scope.joinable.creator,
                    competition: scope.joinable._id
                }, function(response){
                    scope.sending = false;
                    scope.requestsent = true;
                    if(response.error) {
                        scope.alert = {
                            message: response.error,
                            success: false
                        }
                    }
                })

            }
        }
    }
}])