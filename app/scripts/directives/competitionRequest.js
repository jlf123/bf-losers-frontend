/**
 * Created by john.flournoy on 2/16/17.
 */
'use strict';

angular.module('losers').directive('competitionRequest',[ 'Request', '$rootScope', function(Request, $rootScope){
    return {
        restrict: 'E',
        scope: {
            request: '='
        },
        templateUrl: 'templates/competitionrequest.html',
        link: function(scope, el, attr){
            scope.approved = null;
            scope.approve = function(choice){
                scope.approved = choice;
                Request.update({
                    requestId: scope.request._id
                }, { approve: choice }, function(response){
                    $rootScope.$broadcast('event:requestchecked')
                })
            }
        }
    }
}])