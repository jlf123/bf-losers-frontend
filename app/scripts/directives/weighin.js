'use strict';

angular.module('losers').directive('weighIn', [ '$rootScope', function($rootScope){
    return {
        restrict: 'E',
        scope: {
            user: '='
        },
        templateUrl: 'templates/weighin-indv.html',
        link: function($scope, element, attr){

            $scope.saveWeight = function(user){
                $rootScope.$broadcast('event:saveweight', user)
            }

        }
    }
}])