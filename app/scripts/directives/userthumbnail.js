'use strict';

angular.module('losers').directive('userThumbnail', function(){
    return {
        restrict: 'E',
        scope: {
            user: '='
        },
        templateUrl: 'templates/participantthumbnail.html'
    }
})
