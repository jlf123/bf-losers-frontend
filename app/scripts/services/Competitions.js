'use strict';

angular.module('losers').factory('Competitions', ['BASE_URI', '$resource', function(BASE_URI, $resource){
    return $resource(BASE_URI + '/losers/Competitions/:competitionId', {}, {
        query: {
            method: 'GET',
            isArray: true
        }
    })
}])