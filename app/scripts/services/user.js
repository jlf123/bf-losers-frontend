'use strict';

angular.module('losers').factory('Users', ['$resource', 'BASE_URI', function($resource, BASE_URI){
    return $resource(BASE_URI + '/Users', {}, {
        query: {
            method: 'GET',
            isArray: true
        }
    });
}])