'use strict';


angular.module('losers')
    .factory('ProfileData', ['Profile', '$q', function(Profile, $q){

        var profile,
            deferred;

        var clear = function(){
            profile = null;
            deferred = null;
        }

        var abort = function(){
            var abortDeferred = $q.defer()
            abortDeferred.resolve();
            return abortDeferred.promise
        }

        var loadProfile = function(){
            return Profile.get({}).$promise.then(function(result){
                profile = result
            })
        }

        var getProfile = function(){
            if(!deferred && !profile){
                deferred = $q.defer();
                return loadProfile()
                    .then(function(){
                        //if here and deferred is null then service was cleared, abort ajax call
                        if(!deferred){ return abort(); }
                        deferred.resolve(profile);
                        return deferred.promise;
                    })
            } else {

                if(!deferred){
                    // if deferred is null but profile is defined then clear() was called before ajax request finished
                    // we must return an empty promise to ensure acceptance criteria of clear is accepted
                    return abort()

                }
                if(!deferred.promise.$$state.status){ return deferred.promise; }
                deferred.resolve(profile);
                return deferred.promise
            }
        }

        return {
            getProfile: getProfile,
            clear: clear
        }

    }]);