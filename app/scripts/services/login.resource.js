angular.module('losers')
    .factory('Login', [ '$resource', 'BASE_URI',
        function($resource, BASE_URI){
            return $resource(BASE_URI + '/signin', {}, {
                save : {
                    method: 'POST'
                }

            });

    }])
