'use strict';

angular.module('losers').factory('IndividualWeighin', ['$resource', 'BASE_URI', function($resource, BASE_URI) {
    return $resource(BASE_URI + '/losers/IndividualWeighin/:indvWeighinId', {}, {
        update: {
            method: 'PUT'
        }
    })

}])