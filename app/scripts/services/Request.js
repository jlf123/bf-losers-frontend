'use strict';

angular.module('losers').factory('Request', ['$resource', 'BASE_URI', function($resource, BASE_URI) {
    return $resource(BASE_URI + '/losers/Request/:requestId', {}, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            isArray: true
        }
    })

}])

/*
decider: owner._id
competition: competition._id
 */