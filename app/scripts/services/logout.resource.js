/**
 * Created by john.flournoy on 9/19/16.
 */

'use strict';

angular.module('losers').factory('Logout', ['$resource', 'BASE_URI',
    function($resource, BASE_URI){
        return $resource(BASE_URI + '/signout')
    }])