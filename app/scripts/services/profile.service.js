angular.module('losers')
    .factory('Profile', ['BASE_URI', '$resource', function(BASE_URI, $resource){
        return $resource(BASE_URI + '/Profile/:id', {}, {
            save : {
                method: 'POST'
            }

        });

    }]);