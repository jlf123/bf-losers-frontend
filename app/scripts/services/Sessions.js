angular.module('losers').factory('Sessions', ['$resource', 'BASE_URI', function($resource, BASE_URI){
    return $resource(BASE_URI + '/Sessions');
}])