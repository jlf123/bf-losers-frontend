'use strict';

angular.module('losers').factory('Weighin', ['$resource', 'BASE_URI', function($resource, BASE_URI) {
    return $resource(BASE_URI + '/losers/Competition/:competitionId/Weighin/:weighinId', {}, {
        update: {
            method: 'PUT'
        }
    })

}])