'use strict';

angular.module('losers').factory('Query', ['$resource', 'BASE_URI', function($resource, BASE_URI) {
    return $resource(BASE_URI + '/losers/Query', {}, {
        update: {
            method: 'PUT'
        },
        query: {
            method: 'GET',
            isArray: true
        }
    })

}])

