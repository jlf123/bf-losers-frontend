'use strict';

angular.module('losers').service('JWTInterceptor', ['localStorageService', function(localStorageService){
    this.request = function(config){
        config.headers['FlournoyProd-JWT'] = localStorageService.get('JWT-Key')
        return config;
    }
}])